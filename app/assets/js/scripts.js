/*!
 * gulp-starter
 * 
 * 
 * @author 
 * @version 1.0.5
 * Copyright 2018. MIT licensed.
 */
(function ($, window, document, undefined) {

    'use strict';

    $(function () {

        // попап меню записи на тест драйв в хедере на мобиле
        (function () {
            $('.js-popup').on('click', function (event) {
                event.preventDefault();
                $('.popup-overlay').toggleClass('js-active');
                $('.call-block').toggleClass('js-open');
                $('body').css('overflow', 'hidden');
            });
            $('.js-close').on('click', function (event) {
                event.preventDefault();
                $('.call-block').toggleClass('js-open');
                $('.popup-overlay').toggleClass('js-active');
                $('body').css('overflow', 'auto');
            });
        })();

        // попап меню навигации по сайту в хедере на мобиле
        (function () {
            $('.js-popup-menu').on('click', function (event) {
                event.preventDefault();
                $('.popup-overlay_position-left').toggleClass('js-active');
                $('.mobile-menu').toggleClass('js-open');
                $('body').css('overflow', 'hidden');
            });
            $('.js-close-menu').on('click', function (event) {
                event.preventDefault();
                $('.mobile-menu').toggleClass('js-open');
                $('.popup-overlay_position-left').toggleClass('js-active');
                $('body').css('overflow', 'auto');
            });
        })();

        // аккардеон в меню навигации по сайту в хедере на мобиле
        $('.toggle').click(function (e) {
            e.preventDefault();

            let $this = $(this);

            if ($this.next().hasClass('show')) {
                $this.next().removeClass('show');
                $this.next().slideUp(350);
                $this.removeClass('active');
            } else {
                $this.parent().parent().find('.inner').removeClass('show');
                $this.parent().parent().find('.inner').slideUp(350);
                $this.next().toggleClass('show');
                $this.next().slideToggle(350);
                $this.parent().parent().find('.toggle').removeClass('active');
                $this.toggleClass('active');
            }
        });

        // фиксируем меню на телефоне и фикс высоты
        (function () {
            if ($(window).width() <= 992) {
                $(window).bind('scroll', function () {
                    if ($(window).scrollTop() > 132) {
                        $('#menu-wrapper').addClass('fixed');
                        $('body').css('padding-top', 40);
                    } else {
                        $('#menu-wrapper').removeClass('fixed');
                        $('body').css('padding-top', 0);
                    }
                });
            }
        })();

        // табы на странице просмотра авто
        (function () {
            const CurrentBox = $('.tab-box');
            CurrentBox.hide();
            CurrentBox.first().show();

            $('.js-tabs').find('a').on('click', function (e) {
                e.preventDefault();
                $('.js-tabs').find('.current').removeClass('current');
                $(this).addClass('current');
                $(this.hash).show().siblings().hide();
            });

        })();

        // табы в табах странице просмотра авто
        (function () {
            const CurrentBox = $('.sub-tab-box');
            CurrentBox.hide();
            CurrentBox.first().show();

            $('.js-sub-tabs').find('a').on('click', function (e) {
                e.preventDefault();
                $('.js-sub-tabs').find('.current').removeClass('current');
                $(this).addClass('current');
                $(this.hash).show().siblings().hide();
            });

        })();

        //комлектация в мобильной версии
        $('.list-item__title').click(function () {
            // arrow animation on click
            $(this).children('.arrow').toggleClass('active');
            $(this).parent().siblings().children('.list-item__title').children('.arrow').removeClass('active');

            // slide down animation body
            $(this).next('.list-item__info-wrap').slideToggle('fast').toggleClass('active');
            $(this).parent().siblings().children('.list-item__info-wrap').slideUp('fast');
        });

        // аккордион на странице просмотра авто
        (function () {
            $('.accordion-header').click(function () {
                // arrow animation on click
                $(this).children('.arrow').toggleClass('active');
                $(this).parent().siblings().children('.accordion-header').children('.arrow').removeClass('active');

                // slide down animation body
                $(this).next('.accordion-body').slideToggle('fast').toggleClass('active');
                $(this).parent().siblings().children('.accordion-body').slideUp('fast');
            });
        })();

        //скролл по категориям на странице
        (function () {
            function goToByScroll(id) {
                // Reove "link" from the ID
                id = id.replace('link', '');
                // Scroll
                $('html,body').animate({
                    scrollTop: $('#' + id).offset().top
                },'slow');
            }

            $('#sidebar > ul > li > a').click(function (event) {
                event.preventDefault();
                // Call the scroll function
                goToByScroll($(this).attr('id'));
            });
        })();

        // слайдер на странице просмотра машины
        (function () {
            let menu = ['Звук', 'Volvo On Call', 'Навигация', 'Органы управления'];
            let swiper = new Swiper('.swiper-container-auto-view', {
                parallax: true,
                slidesPerView: 'auto',
                centeredSlides: true,
                loop: true,
                loopedSlides: 4,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                    renderBullet: function (index, className) {
                        return '<span class="' + className + '">' + (menu[index]) + '</span>';
                    },

                },
            });
        })();

        // слайдер на странице auto-selekt для всех блоков с авто
        $('.js-slider-auto-selekt').each(function (index, element) {

            let galleryTop = new Swiper($(element).find('.gallery-top'), {
                spaceBetween: 10,
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
                loop: true,
                loopedSlides: 4
            });

            let galleryThumbs = new Swiper($(element).find('.gallery-thumbs'), {
                centeredSlides: true,
                slidesPerView: 'auto',
                touchRatio: 0.2,
                slideToClickedSlide: true,
                loop: true,
                loopedSlides: 4
            });

            galleryTop.controller.control = galleryThumbs;
            galleryThumbs.controller.control = galleryTop;
        });

        //маска на телефон
        (function () {
            $('.js-number-mask').mask('+7 999 999 99 99');
        })();

        // //nice select
        (function () {
            $('select').niceSelect();
        })();

        // галерея на странице авто
        (function () {
            $(window).resize(function () {
                if ($(window).width() <= 768) {
                    $('.fancybox').fancybox();
                }
            });
        })();

    });

})(jQuery, window, document);
